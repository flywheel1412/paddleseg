#! /bin/bash

export CUDA_VISIBLE_DEVICES=0,1,2,3
export LD_LIBRARY_PATH=/usr/local/python3.7.0/lib:$LD_LIBRARY_PATH

SAVE_DIR=segmentation_train/fcn_hrnetw18_organ_1024x1024_bs4_7-17

python -m paddle.distributed.launch \
    train.py \
    --config configs/fcn/fcn_hrnetw18_cityscapes_1024x512_80k_hubmapOrgan.yml \
    --do_eval \
    --use_vdl \
    --save_interval 500 \
    --save_dir ${SAVE_DIR} \
    | tee ${SAVE_DIR}/train.log
