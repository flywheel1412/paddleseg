import pandas as pd
import numpy as np
import os

csvfile = "../../input/hubmap-organ-segmentation/train.csv"
image_dir = "hubmap_organ/train_images"
label_dir = "hubmap_organ/train_labels"

data = pd.read_csv(csvfile)

data_dict = []
for i in range(data.shape[0]):
    data_dict.append([data.iloc[i]['id'].item(), data.iloc[i]['organ']])

np.random.shuffle(data_dict)
np.random.seed(3457)
np.random.shuffle(data_dict)

names = ['kidney', 'largeintestine', 'lung', 'prostate', 'spleen']
count = [0] * len(names)
train, val = [], []

for i in range(len(data_dict)):
    idx = names.index(data_dict[i][1])
    if count[idx] < 11:
        val.append(os.path.abspath(os.path.join(image_dir, f"{data_dict[i][0]}.tiff")))
        count[idx] += 1
    else:
        train.append(os.path.abspath(os.path.join(image_dir, f"{data_dict[i][0]}.tiff")))
    
with open(f"hubmap_organ/train.txt", 'w') as f:
    f.write("\n".join(train))

with open(f"hubmap_organ/val.txt", 'w') as f:
    f.write("\n".join(val))
