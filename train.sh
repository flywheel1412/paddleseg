#! /bin/bash

export CUDA_VISIBLE_DEVICES=0
export LD_LIBRARY_PATH=/usr/local/python3.7.0/lib:$LD_LIBRARY_PATH

python -m paddle.distributed.launch train.py \
    --config configs/fcn/fcn_hrnetw18_cityscapes_1024x512_80k.yml \
    --do_eval \
    --use_vdl \
    --save_interval 500 \
    --save_dir segmentation_train/fcn_hrnetw18_960x960_7-10
