from glob import glob
import cv2
import json
import os, shutil
import numpy as np
import pandas as pd
from tqdm import tqdm


def rle2mask(mask_rle, label, shape=(1600, 256)):
    '''
    mask_rle: run-length as string formated (start length)
    shape: (width,height) of array to return 
    Returns numpy array, 1 - mask, 0 - background

    '''
    s = mask_rle.split()
    starts, lengths = [np.asarray(x, dtype=int)
                       for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.ones(shape[0]*shape[1], dtype=np.uint8) * 255
    for lo, hi in zip(starts, ends):
        img[lo:hi] = label
    return img.reshape(shape).T


# names = sorted(['prostate', 'spleen', 'lung', 'kidney', 'largeintestine'])
names = ['kidney', 'largeintestine', 'lung', 'prostate', 'spleen']
print(f"names: {names}")


def process(csvfile, train_image_dir):
    label_save_dir = "./hubmap_organ/train_labels"
    image_save_dir = label_save_dir.replace("labels", "images")

    if not os.path.exists(label_save_dir):
        os.makedirs(label_save_dir)
    if not os.path.exists(image_save_dir):
        os.makedirs(image_save_dir)

    image_ls = sorted(glob(train_image_dir + "/*tiff"))
    data = pd.read_csv(csvfile)

    for imagename in tqdm(image_ls):
        idname = int(imagename.split("/")[-1].split(".")[0])
        h, w = cv2.imread(imagename).shape[:-1]
        idx = data['id'] == idname
        # print(idname, idx, sep=' ')
        # print(data[idx]['organ'].item())
        n = names.index(data[idx]['organ'].item())
        # print(data.loc[idx]['rle'])
        mask = rle2mask(data[idx]['rle'].item(), n, (w, h))

        savename = os.path.join(label_save_dir, f"{idname}.png")
        # print(savename)
        cv2.imwrite(savename, mask)
        
        shutil.copy(imagename, image_save_dir)


process("../../input/hubmap-organ-segmentation/train.csv", 
        "../../input/hubmap-organ-segmentation/train_images")
# process("hubmap-organ/test.csv", "hubmap-organ/test_images")
