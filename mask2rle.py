#!usr/bin/env python  
# -*- coding:utf-8 -*-  

from glob import glob
import numpy as np

image_ls = glob("results/*png")


def rle_encode_less_memory(img):
  '''
  img: numpy array, 1 - mask, 0 - background
  Returns run length as string formated
  This simplified method requires first and last pixel to be zero
  '''
  pixels = img.T.flatten()

  # This simplified method requires first and last pixel to be zero
  pixels[0] = 0
  pixels[-1] = 0
  runs = np.where(pixels[1:] != pixels[:-1])[0] + 2
  runs[1::2] -= runs[::2]

  return ' '.join(str(x) for x in runs)


