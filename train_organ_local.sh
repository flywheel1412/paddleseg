#! /bin/bash

export CUDA_VISIBLE_DEVICES=0,1,2,3
export LD_LIBRARY_PATH=/usr/local/python3.7.0/lib:$LD_LIBRARY_PATH

python -m paddle.distributed.launch \
    train.py \
    --config configs/fcn/fcn_hrnetw18_cityscapes_1024x512_80k_hubmapOrgan_local.yml \
    --do_eval \
    --use_vdl \
    --save_interval 500 \
    --save_dir segmentation_train/fcn_hrnetw18_organ_1024x1024_bs8_7-10
