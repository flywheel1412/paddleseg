#! /bin/bash

export CUDA_VISIBLE_DEVICES=0
export LD_LIBRARY_PATH=/usr/local/python3.7.0/lib:$LD_LIBRARY_PATH

python -m paddle.distributed.launch --selected_gpus='0' \
    predict.py \
    --config configs/fcn/fcn_hrnetw18_cityscapes_1024x512_80k_hubmapOrgan_local.yml \
    --image_path ../../input/hubmap-organ-segmentation/test_images \
    --model_path model.pdparams
